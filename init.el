;;; init-org.el ---
(require 'package)
(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/") t)

(setq package-list
      '(all-the-icons magit ein auto-complete-auctex auto-complete treemacs treemacs-magit treemacs-all-the-icons rainbow-mode counsel doom-themes centaur-tabs visual-fill-column spaceline diff-hl spacemacs-theme rainbow-delimiters one-themes org-pdftools latex-preview-pane auto-complete-c-headers emojify auctex wolfram highlight-parentheses yasnippet autopair org-bullets org))

(package-initialize)

;; Install any missing packages
(unless package-archive-contents
  (package-refresh-contents))

(dolist (package package-list)
  (unless (package-installed-p package)
    (package-install package)))

;; Don't ask about git controlled symlinks
(setq vc-follow-symlinks nil)
;; Don't ask about org mode config files
(setq org-confirm-babel-evaluate nil)

;; Load our Org-Mode configuration file
(org-babel-load-file
 (expand-file-name
  "config.org"
  user-emacs-directory))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("01cf34eca93938925143f402c2e6141f03abb341f27d1c2dba3d50af9357ce70" "843254de2161a32f8827a67cf43311d15e2dd8e3c371482a3c86e2b7815b81ef" "9efb2d10bfb38fe7cd4586afb3e644d082cbcdb7435f3d1e8dd9413cbe5e61fc" "76bfa9318742342233d8b0b42e824130b3a50dcc732866ff8e47366aed69de11" "89d7f4cfd96f400c959300711bd783a7f0226706ccda8b6920237f5a999ed212" "0f8ebf6d31bb7d21a449a151f2560b74fae957a664cdde02dc0d428e4248bf23" "902215108708dc125696cce6d51d7ba7b03908a5f73ae4ad13b647d2a6550303" "563ad08c104c7b61d7d4aeec1a4dc7a59b092b96a39387a8f08e95182a1b786d" "f2954ffdd56d58db52c94e23ca590a4a408a19126b20ad49d67efce39c9e7b65" "f55256e9428bca9e486e41838c157aaaa423d52bbea1db4fd8bfc7f9c2d801ba" "28548f98d832264f1b03119be8bdd8ecce5abf49341ae5c335b1075a57423f62" "9097f55f195285b869969e1b490d1d0993977ff34062583c5f19af45f203c69e" "2f1518e906a8b60fac943d02ad415f1d8b3933a5a7f75e307e6e9a26ef5bf570" "79278310dd6cacf2d2f491063c4ab8b129fee2a498e4c25912ddaa6c3c5b621e" "fa2b58bb98b62c3b8cf3b6f02f058ef7827a8e497125de0254f56e373abee088" "0dd2666921bd4c651c7f8a724b3416e95228a13fca1aa27dc0022f4e023bf197" "37768a79b479684b0756dec7c0fc7652082910c37d8863c35b702db3f16000f8" "bffa9739ce0752a37d9b1eee78fc00ba159748f50dc328af4be661484848e476" "b73a23e836b3122637563ad37ae8c7533121c2ac2c8f7c87b381dd7322714cd0" "dcdd1471fde79899ae47152d090e3551b889edf4b46f00df36d653adc2bf550d" default))
 '(hl-todo-keyword-faces
   '(("TODO" . "#dc752f")
     ("NEXT" . "#dc752f")
     ("THEM" . "#2d9574")
     ("PROG" . "#4f97d7")
     ("OKAY" . "#4f97d7")
     ("DONT" . "#f2241f")
     ("FAIL" . "#f2241f")
     ("DONE" . "#86dc2f")
     ("NOTE" . "#b1951d")
     ("KLUDGE" . "#b1951d")
     ("HACK" . "#b1951d")
     ("TEMP" . "#b1951d")
     ("FIXME" . "#dc752f")
     ("XXX+" . "#dc752f")
     ("\\?\\?\\?+" . "#dc752f")))
 '(org-agenda-files '("~/Nextcloud/todo.org"))
 '(org-latex-compiler "lualatex")
 '(org-startup-indented t)
 '(package-selected-packages
   '(ein auto-complete-auctex auto-complete treemacs-magit treemacs-all-the-icons rainbow-mode counsel magit all-the-icons doom-themes centaur-tabs treemacs visual-fill-column spaceline diff-hl spacemacs-theme rainbow-delimiters one-themes org-pdftools latex-preview-pane auto-complete-c-headers emojify auctex wolfram highlight-parentheses yasnippet autopair org-bullets org))
 '(pdf-view-midnight-colors '("#b2b2b2" . "#292b2e")))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (::weight light :height 120 :width normal :family "Ubuntu Mono" :foundry "unknown" :slant normal :weight normal)))))
