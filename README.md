# Emacs Configuration
Switching to Emacs can be a pretty daunting task, the keyboard shortcuts are unfamiliar, the look and feel of out of the box emacs looks quite dated, and the configuration is primarily done through a programming language unfamiliar to most (emacs-lisp).

This configuration seeks to solve those problems. Some hotkeys have been modified to be more familiar to most people. Additionally,   there are many changes to Emacs' original design, aimed at making the editior look more modern and minimal. But best of all, the configuration file is setup as an Org-Mode file with instructions, and easily understood text allowing the user to easily select the features they would like to incorporate and those that they would like to disregard.

![Config File](resources/demo.png)

## Installation
To install this configuration just run the following, beginning with backing up your current `init.el` and `config.el` (if you have one).
```bash
mv ~/.emacs.d/init.el ~/.emacs.d/init.el.bak
mv ~/.emacs.d/config.el ~/.emacs.d/config.el.bak
```
I recommend you then `clone` this repository to another location of your choosing, then symlink the relevant files to your `~/.emacs.d` directory as follows:
```bash
git clone https://gitlab.com/watermelonvotary/emacs.git ~/
ln -snf ~/emacs/* ~/.emacs.d/
```

## How to use
Simply load the configuration file in emacs and look through the Guide section.
```bash
emacs ~/.emacs.d/config.org
```

## License
[GNU GPLv3](https://gitlab.com/watermelonvotary/emacs/-/blob/master/LICENSE)